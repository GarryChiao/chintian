<!--Information section-->
<section id="information">
  <div class="information-div">
    <!-- this div for icon -->
    <div class="information-navigate-icon" align="center">
      <img src="img/information/navigate-icon.svg" class="img-responsive" alt="">
    </div>
    <!-- this div for information -->
    <div class="information-information NotoSerif-Medium">
      <h3>【服務時間】</h3>
      <h4>週一至週日</h4>
      <h4>
        午餐 / 11:30-14:00<br>
        午茶 / 14:30-17:00<br>
        晚餐 / 17:30-21:00<br>
      </h4>
      <p>
        ◎每月第一個星期一休館<br>
        ◎為保護古蹟檜木地板，請著襪入內
      </p>
      <div class="spacer"></div>
      <h3>【訂位電話】</h3>
      <h4>(02)2391-6676 <small>(11:00開始接聽)</small> </h4>
      <div class="spacer"></div>
      <h3>【交通位置】</h3>
      <h4>10649 台北市大安區青田街7巷6號</h4>
      <div class="information-travel-guide">
        <p>
          ◎公車&nbsp;/&nbsp;溫州街口站，步行約5分鐘
        </p>
        <p>
          ◎捷運&nbsp;/&nbsp;東門站5號出口，步行約15分鐘<br>
          &emsp;&emsp;&emsp;&emsp;大安森林公園站2號出口，步行約15分鐘
        </p>
      </div>
    </div>
    <div class="information-map">
      <iframe width="100%" frameborder="0.5" src="https://www.google.com/maps/embed/v1/place?q=10649%E5%8F%B0%E5%8C%97%E5%B8%82%E5%A4%A7%E5%AE%89%E5%8D%80%E9%9D%92%E7%94%B0%E8%A1%977%E5%B7%B76%E8%99%9F&key=AIzaSyAxFKPDxeSAiUtzmH2q_-HkBweLxpsuirc" allowfullscreen></iframe>
    </div>
    <!-- footer -->
    <div class="information-footer NotoSans-Light">
      <hr>
      <div class="information-footer-icon">
        <img src="img/information/footer-icon.png" class="img-responsive" alt="">
      </div>
      <div class="information-footer-intro">
        <h5 class="NotoSerif-Medium">國立臺灣大學日式宿舍─馬廷英故居</h5>
        <p class="NotoSans-Regular">
          National Taiwan University<br>
          Japanese Dormitory─Ma Ting-Ying Residence
        </p>
      </div>
      <div class="information-footer-link">
        <a href="https://qingtian76.tw/cht/index.php">官方網站</a>
        |
        <a href="https://www.facebook.com/geo76.tw/">FACEBOOK</a>
      </div>
    </div>
  </div>
</section>
