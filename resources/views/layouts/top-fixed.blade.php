<div class="fixed-top-logo">
  <img src="img/home/logo.png" class="img-responsive" alt="">
  <div class="fixed-top-reserve-btn visible-xs visible-sm">
    <a class="reserve-nav-btn" href="#reserve">
      用餐訂位
      <img src="img/home/reserve-btn.png" class="img-responsive">
    </a>
  </div>
</div>
<div class="fixed-top-links NotoSans-Light hidden-xs hidden-sm">
  <!-- Single button -->
  <div class="btn-group">
    <button type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      繁體中文 <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
      <li><a href="#">繁體中文</a></li>
      <li><a style="pointer-events:none; color:#777;">English (建置中)</a></li>
    </ul>
  </div>
  |
  <a href="https://qingtian76.tw/cht/index.php">官網</a>
  |
  <a href="https://www.facebook.com/geo76.tw/"><i class="fab fa-facebook-f"></i></a>
</div>
