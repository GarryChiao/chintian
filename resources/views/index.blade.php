<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NZX5XFS');</script>
    <!-- End Google Tag Manager -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- rippler CSS -->
    <link rel="stylesheet" href="{{ asset('css/rippler.min.css') }}">
    <!-- <link rel="stylesheet" href="{{ asset('css/datepicker.css') }}"> -->
    <!-- use fontawesome icons with cdn -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <!-- import fonts from google fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700|Noto+Serif:400,700" rel="stylesheet"> -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <title>ChinTian</title>
  </head>
  <body data-spy="scroll" data-target="#navbar">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NZX5XFS"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="w-100">
      @include('layouts.top-fixed')
      @include('sec1-home')
      @include('layouts.navbar')
      @include('sec2-story')
      @include('sec3-cuisine')
      @include('sec4-memory')
      @include('sec5-reserve')
      @include('sec6-information')
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script language="JavaScript" type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- chintian font family -->
    <script src="//s3-ap-northeast-1.amazonaws.com/justfont-user-script/jf-55687.js"></script>
    <script language="JavaScript" type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <script language="JavaScript" type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script language="JavaScript" type="text/javascript" src="{{ asset('js/datepicker.js') }}"></script>
    <script language="JavaScript" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <!-- rippler js -->
    <script src="{{ asset('js/jquery.rippler.min.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function() {
      $(".rippler").rippler({
        effectClass      :  'rippler-effect'
        ,effectSize      :  16      // Default size (width & height)
        ,addElement      :  'div'   // e.g. 'svg'(feature)
        ,duration        :  500
      });
    });
    </script>
  </body>
</html>
