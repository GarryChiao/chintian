<!--Reserve section-->
<section id="reserve">
  <div class="row">
    <div class="col-md-12 col-lg-10 col-lg-offset-1 reserve-bg">
      <div class="reserve-div">
        <!-- this div for icon -->
        <div class="reserve-navigate-icon" align="center">
          <img src="img/reserve/navigate-icon.svg" class="img-responsive" alt="">
        </div>
        <!-- this div for reservation form  -->
        <div class="NotoSerif-Medium reserve-form-div">
          <!-- Title -->
          <h1>請留下您的訂位及聯絡資料 我們將有專人為您服務</h1>
          <!-- this div for reservation form -->
          <div class="reserve-form">
            <form name="reservation-form">
              <div class="input-item" style="margin-top: 0px;">
                <label for="name" class="">姓&emsp;&emsp;名</label>
                <input class="form-control" name="name" type="text" id="name" placeholder="必填">
              </div>
              <div class="input-item">
                <label for="contact-phone">聯絡電話</label>
                <input class="form-control" name="contact-phone"  type="text" id="contact-phone" placeholder="必填，外國籍人士請+國碼">
              </div>
              <div class="input-item">
                <label for="email">電子郵件</label>
                <input class="form-control" name="email" type="email" id="email" placeholder="必填">
              </div>
              <div class="input-item">
                <label for="count">人&emsp;&emsp;數</label>
                <input class="form-control" name="count" type="number" id="count" min="0" step="1" placeholder="必填">
              </div>
              <div class="input-item">
                <label for="date">日&emsp;&emsp;期</label>
                <input name="date" class="form-control" type="text" id="date" placeholder="必填">
              </div>
              <div class="input-item">
                <label for="time">時&emsp;&emsp;段</label>
                <select name="time" class="form-control">
                  <option value="午餐 / 11:30-14:00">午餐 / 11:30-14:00</option>
                  <option value="午茶 / 14:30-17:00">午茶 / 14:30-17:00</option>
                  <option value="晚餐 / 17:30-21:00">晚餐 / 17:30-21:00</option>
                </select>
              </div>
              <div class="input-item">
                <label for="note">備&emsp;&emsp;註</label>
                <textarea name="note" class="form-control" rows="3"></textarea>
              </div>
              <div class="input-item">
                <label for="confirm">驗&emsp;&emsp;證</label>
                <div class="reserve-g-captcha">
                  <!-- <input type="text" class="form-control" id="confirm"> -->
                  <div class="g-recaptcha" data-sitekey="6LfmKloUAAAAAKE1K25ibdJWExUVUWlyc6MGcCp4"></div>
                </div>
              </div>
            </form>
          </div>
          <!-- reverve information -->
          <div class="reserve-info">
            <h4>【訂位說明】</h4>
            <p>請留意在您填完表格發送後，尚未完成訂位，我們會儘快在館內服務時間(11:30～21:00)致電與您聯繫，以確認訂位狀況，請稍待！</p>
          </div>
          <!-- submit button -->
          <div class="reserve-form-submit">
            <button type="button" name="submit" class="rippler rippler-default information-description-icon" data-toggle="modal" id="make-reservation">
              確認送出
            </button>
            <!-- Modal -->
            <div class="modal fade" id="sweetAlertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body" align="center">
                    <div class="sweet-alert" align="center">
                      <button type="button" class="sweet-alert-modal-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <!-- <div class="green-border" align="center">
                        <h4>貼心提醒</h4>
                        <p>
                          您的訂位需求已經成功寄出，我們會儘快在館內服務時間（11:30～21:00）致電與您聯繫，請稍待！<br>
                          若無法等候，煩請於開館期間來電02-23916676親自洽詢，謝謝！
                        </p>
                      </div> -->
                      <img src="{{ asset('img/reserve/alert.png') }}" class="img-responsive" alt="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end -->
        </div>
      </div>
    </div>
  </div>
</section>
