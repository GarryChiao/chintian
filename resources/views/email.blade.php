<div class="">
  <ul>
    <li>姓名： {{ $reservation->name }}</li>
    <li>聯絡電話： {{ $reservation->phone }}</li>
    <li>電子郵件： {{ $reservation->email }}</li>
    <li>人數： {{ $reservation->attendance }}</li>
    <li>日期： {{ $reservation->date }}</li>
    <li>時段： {{ $reservation->time }}</li>
    <li>備註： {{ $reservation->note }}</li>
  </ul>
</div>
