<!--Cuisine section-->
<section id="cuisine">
  <div class="row section-row">
    <!-- the carousel part -->
    <div class="left-carousel">
      <div id="carousel-example-cuisine" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-cuisine" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-cuisine" data-slide-to="1"></li>
          <li data-target="#carousel-example-cuisine" data-slide-to="2"></li>
          <li data-target="#carousel-example-cuisine" data-slide-to="3"></li>
          <li data-target="#carousel-example-cuisine" data-slide-to="4"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="img/cuisine/slide-1.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/cuisine/slide-2.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/cuisine/slide-3.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/cuisine/slide-4.jpg" alt="...">
          </div>
          <div class="item">
            <img src="img/cuisine/slide-5.jpg" alt="...">
          </div>
        </div>
      </div>
    </div>
    <div class="right-information col-md-5 col-md-offset-7" align="center">
      <div class="">
        <div class="col-md-4 col-md-offset-4 col-sm-12 col-sm-offset-0 information-icon">
          <img src="img/cuisine/navigate-icon.svg" class="img-responsive" alt="">
        </div>
      </div>
      <div class="information-title NotoSerif-Semi">
        <h1>好&nbsp;食•好&nbsp;時</h1>
      </div>
      <div class="information-description NotoSerif-Medium" align="left">
        <h2>不管是一個人的悠閒午茶，或招朋引伴共聚豐盛的餐桌，邀請您在老屋的餐桌上品嚐用心烹調的季節滋味，一同享受美好的溫暖時光。</h2>
      </div>
      <div class="information-description-icon-div" align="center">
        <div class="information-description-icon">
          <button type="button" class="information-description-icon button-left" data-toggle="modal" data-target="#cuisineModal">
            菜單一覽
            <i class="fas fa-chevron-circle-right"></i>
          </button>
          <!-- Modal -->
          <div class="modal fade" id="cuisineModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-body">
                  <div class="cuisine-modal">
                    <button type="button" class="cuisine-modal-close" data-dismiss="modal" aria-label="Close">
                      <img src="img/close.png" class="img-responsive" alt="">
                    </button>
                    <button class="btn NotoSerif-Semi cuisine-modal-btn" type="button" onclick="showMenu('lunch')">午&emsp;餐</button>
                    <button class="btn NotoSerif-Semi cuisine-modal-btn" type="button" onclick="showMenu('afternoonTea')">午&emsp;茶</button>
                    <button class="btn NotoSerif-Semi cuisine-modal-btn" type="button" onclick="showMenu('dinner')">晚&emsp;餐</button>
                    <button class="btn NotoSerif-Semi cuisine-modal-btn" type="button" onclick="showMenu('banquet')">預約會席料理</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <button type="button" class="information-description-icon button-right reserve-nav-btn">
            用餐訂位
            <i class="fas fa-chevron-circle-right"></i>
          </button>
        </div>
      </div>
    </div>
  </div>
  <!-- menu div -->
  <div id="lunch">
    <img src="img/cuisine/menu/m-lunch.jpg" class="img-responsive" alt="lunch-img">
    <button type="button" onclick="closeMenu('lunch')" name="button" class="btn btn-default"> X </button>
  </div>
  <div id="afternoonTea">
    <img src="img/cuisine/menu/m-after.jpg" class="img-responsive" alt="afternoonTea-img">
    <button type="button" onclick="closeMenu('afternoonTea')" name="button" class="btn btn-default"> X </button>
  </div>
  <div id="dinner">
    <img src="img/cuisine/menu/m-dinner.jpg" class="img-responsive" alt="dinner-img">
    <button type="button" onclick="closeMenu('dinner')" name="button" class="btn btn-default"> X </button>
  </div>
  <div id="banquet">
    <img src="img/cuisine/menu/m-ban.jpg" class="img-responsive" alt="banquet-img">
    <button type="button" onclick="closeMenu('banquet')" name="button" class="btn btn-default"> X </button>
  </div>
</section>
