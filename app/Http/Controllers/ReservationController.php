<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use Illuminate\Support\Facades\Mail;

class ReservationController extends Controller
{
     public function store(Request $request) {

        $RECAPTCHA_SECRET = env("GOOGLE_RECAPTCHA_SECRET");
        $verify=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$RECAPTCHA_SECRET}&response={$request->captcha}");
        $captcha_success=json_decode($verify);

        if($captcha_success->success==false) {
            return response()->json(['error'=>'Google recaptha is not verified']);
        }
        else if ($captcha_success->success==true) {
            $reservation = new Reservation();
            $reservation->name = $request->name;
            $reservation->phone = $request->phone;
            $reservation->email = $request->email;
            $reservation->attendance = $request->attendance;
            $reservation->date = $request->date;
            $reservation->time = $request->time;
            $reservation->note = $request->note;

            $reservation->save();

            Mail::send('email', ['reservation' => $request], function ($m) use ($request) {
                // $emails = ['andynumber10@gmail.com', 'rossi.chen@syincheng.com',env('MAIL_USERNAME')];
                // $emails = ['rossi.chen@syincheng.com', env('MAIL_USERNAME')];
                // $emails = ['eggrollick@gmail.com',env('MAIL_USERNAME')];
                $emails = env('MAIL_USERNAME');
                $m->from(env("MAIL_USERNAME"), $request->name);
                $m->to($emails)->subject('您有新的訂位！');
            });

            return response()->json(['success'=>'Data is successfully added'], 200);
        }
    }
}
